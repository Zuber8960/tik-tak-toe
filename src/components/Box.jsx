import React from 'react'

 const Box = ({cb, index, cell}) => {
  return (
    <>
    <div className="box" onClick={() => cb(index)}>
          {cell}
    </div>
    </>
  )
};


export default Box;