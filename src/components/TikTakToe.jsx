import React, { useState } from "react";
import Box from "./Box.jsx";

const TikTakToe = () => {
  const [player, setPlayer] = useState("X");
  const [arr, setArr] = useState(new Array(9).fill(""));
  const [winner, setGameWinner] = useState("");

  function clickFun(index) {
    if (winner === "" && arr[index] === "") {
      if (player === "X" ) {
        setPlayer("O");
      } else if (player === "O") {
        setPlayer("X");
      }
      arr[index] = player;
      setArr(arr);

      checkWinner(arr[index]);
    }
  }

  const winnerCases = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  function checkWinner(winner) {
    const result = winnerCases.some((currPlayer) => {

      
      return (
        arr[currPlayer[0]] === winner &&
        arr[currPlayer[1]] === winner &&
        arr[currPlayer[2]] === winner
      );
    });

    if (result) {
      setGameWinner(winner);
    }
  }

  return (
    <div className="main-container">
      <h1>Tik Tok Toe</h1>
      {winner=== "" ? <h1>Next player : {player}</h1> : <h1>Winner : {winner}</h1>}
      <div className="container">
        {arr.map((cell, index) => {
          return <Box key={index} cb={clickFun} index={index} cell={cell} />;
        })}
      </div>
    </div>
  );
};

export default TikTakToe;
